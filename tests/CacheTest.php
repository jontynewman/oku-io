<?php

namespace JontyNewman\Oku\IO\Tests;

use DateTime;
use DateTimeZone;
use GuzzleHttp\Psr7\ServerRequest;
use JontyNewman\Oku\IO\Cache;
use JontyNewman\Oku\IO\Tests\DirectoryHelper;
use JontyNewman\Oku\ResponseBuilders\RequestHandlerResponseBuilder;
use JontyNewman\Oku\Context\Sessions\RequestHandlerSession;
use JontyNewman\Oku\Contexts\RequestHandlerContext;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use ShrooPHP\Framework\Sessions\Session;

class CacheTest extends TestCase
{
	public function test(): void
	{
		$this->assert();
	}

	public function testWithExtension(): void
	{
		$this->assert('txt');
	}

	public function testDirectory(): void
	{
		$this->assertDirectory(__FUNCTION__);
	}

	public function testDirectoryWithExtension(): void
	{
		$this->assertDirectory(__FUNCTION__, 'txt');
	}

	public function __invoke(string $in, string $out)
	{
		// Do nothing.
	}

	private function assert(string $extension = null): void
	{
		$exception = null;
		$cache = null;
		$invoked = false;
		$path = '/';
		$contents = __FUNCTION__;
		$random = bin2hex(random_bytes(0x10));
		$root = vfsStream::setup(__FUNCTION__);
		$in = $this->toPrefix($root, '/in');
		$prefix = $this->toPrefix($root, '/cache/');
		$parent = rtrim($prefix, '/');

		$callback = function (
			string $actual_in,
			string $out
		) use (&$invoked, $in, $path, $prefix, $extension): void {

			$cache = $this->toCache($prefix, $extension);
			$this->assertSame($in, $actual_in);
			$this->assertSame($cache->directory()[$path], $out);
			$directory = dirname($out);

			if (!file_exists($directory)) {
				$this->assertTrue(mkdir($directory));
			}

			if (file_exists($actual_in)) {
				$this->assertTrue(copy($actual_in, $out));
			}

			$invoked = true;
		};

		$cache = $this->toCache($prefix, $extension, $callback);
		$builder = new RequestHandlerResponseBuilder();
		$request = new ServerRequest('GET', $path);
		$session = new RequestHandlerSession(new Session());
		$context = new RequestHandlerContext($request, $session, '', '', '', '');

		if (file_exists($in)) {
			$this->assertTrue(unlink($in));
		}

		if (file_exists($parent)) {
			rmdir($parent);
		}

		try {
			$cache->__invoke($path, $in, $builder, $context);
		} catch (RuntimeException $exception) {
			$this->assertSame(
				"Unable to build response from '{$cache->directory()[$path]}'",
				$exception->getMessage()
			);
		}

		$this->assertNotNull($exception);
		$this->assertInvoked($invoked);
		$exception = null;

		$this->assertNotFalse(file_put_contents($in, $contents));

		$out = $cache->cache($path, $in);
		$this->assertInvoked($invoked);

		$this->assertSame($contents, file_get_contents($out));

		try {
			$cache->cache($path, $random);
		} catch (RuntimeException $exception) {
			$this->assertSame(
				"Cannot determine modification time for '{$random}'",
				$exception->getMessage()
			);
		}

		$this->assertNotNull($exception);
		$this->assertFalse($invoked);
		$exception = null;

		$cache->__invoke($path, $in, $builder, $context);
		$this->assertFalse($invoked);
		$response = $builder->response();
		$headers = iterator_to_array($response->headers());
		$content = $response->content();

		$finfo = finfo_open(FILEINFO_MIME);
		$type = finfo_file($finfo, $out);
		finfo_close($finfo);

		$this->assertNotFalse($type);
		$this->assertSame($type, $headers['Content-Type'] ?? null);

		$mtime = filemtime($out);
		$format = 'D, d M Y H:i:s \G\M\T';
		$time = DateTime::createFromFormat('U', $mtime, new DateTimeZone('UTC'));
		$this->assertNotFalse($time);
		$formatted = $time->format($format);
		$this->assertTrue(is_string($formatted));
		$this->assertSame($formatted, $headers['Last-Modified'] ?? null);

		$this->assertNotNull($content);

		ob_start();
		$content->run();
		$this->assertSame($contents, ob_get_clean());

		$child = $root->getChild('in');
		$child->lastModified($mtime + 1);
		$cache->__invoke($path, $in, $builder, $context);
		$this->assertInvoked($invoked);
	}

	private function assertDirectory(
		string $root,
		string $extension = null
	): void {
		$prefix = $this->toPrefix(vfsStream::setup($root));
		$cache = $this->toCache($prefix, $extension);
		$actual = $cache->directory();
		DirectoryHelper::assert($this, $actual, '/', $prefix, $extension);
	}

	private function assertInvoked(bool &$invoked): void
	{
		$this->assertTrue($invoked);
		$invoked = false;
	}

	private function toCache(
		string $prefix = null,
		string $extension = null,
		callable $callback = null
	): Cache {

		if (is_null($prefix)) {
			$prefix = $this->toPrefix(vfsStream::setup(__FUNCTION__));
		}

		if (is_null($callback)) {
			$callback = $this;
		}

		return new Cache($callback, $prefix, $extension);
	}

	private function toPrefix(
		vfsStreamDirectory $root,
		string $suffix = '/'
	): string {
		return "{$root->url()}{$suffix}";
	}
}
