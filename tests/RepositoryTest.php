<?php

namespace JontyNewman\Oku\IO\Tests;

use GuzzleHttp\Psr7\ServerRequest;
use GuzzleHttp\Psr7\UploadedFile;
use InvalidArgumentException;
use JontyNewman\Oku\Directory;
use JontyNewman\Oku\IO\Repository;
use JontyNewman\Oku\IO\Tests\DirectoryHelper;
use JontyNewman\Oku\Context\Sessions\RequestHandlerSession;
use JontyNewman\Oku\ContextInterface;
use JontyNewman\Oku\Contexts\RequestHandlerContext;
use JontyNewman\Oku\ResponseBuilders\RequestHandlerResponseBuilder;
use JontyNewman\Oku\ResponseBuilderInterface;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use ShrooPHP\Framework\Sessions\Session;
use org\bovigo\vfs\vfsStreamContent;
use UnexpectedValueException;

class RepositoryTest extends TestCase
{
	public function test()
	{
		$this->assert(vfsStream::setup(__FUNCTION__));
	}

	public function testWithArguments()
	{
		$this->assert(
			vfsStream::setup(__FUNCTION__),
			'txt',
			0700,
			'file',
			'text'
		);
	}

	public function testDirectory()
	{
		$this->assertDirectory(__FUNCTION__);
	}

	public function testDirectoryWithExtension()
	{
		$this->assertDirectory(__FUNCTION__, 'txt');
	}

	public function __invoke(
		string $path,
		string $in,
		ResponseBuilderInterface $builder,
		ContextInterface $context
	): void {

	}

	private function assert(
		vfsStreamDirectory $root,
		string $extension = null,
		int $mode = null,
		string $file = null,
		string $text = null
	): void {

		$exception = null;
		$invoked = false;
		$path = '/';
		$base = $root->url();
		$trimmed = "{$base}/in";
		$prefix = "{$trimmed}/";
		$content = __METHOD__;
		$body = [$text ?? Repository::TEXT => $content];
		$request = (new ServerRequest('POST', $path))->withParsedBody($body);
		$session = new RequestHandlerSession(new Session());
		$directory = new Directory($prefix, $extension);
		$builder = new RequestHandlerResponseBuilder();
		$context = new RequestHandlerContext($request, $session, '', '', '', '');

		$callback = function(
			string $actual_path,
			string $in,
			ResponseBuilderInterface $actual_builder,
			ContextInterface $actual_context
		) use (&$invoked, $path, $directory, $builder, $context): void {

			$this->assertSame($path, $actual_path);
			$this->assertSame($directory->offsetGet($path), $in);
			$this->assertSame($builder, $actual_builder);
			$this->assertSame($context, $actual_context);
			$invoked = true;
		};

		$repo = new Repository(
			$callback,
			$prefix,
			$extension,
			$mode,
			$file,
			$text
		);

		$in = $repo->directory()->offsetGet($path);

		if (is_null($mode)) {
			$mode = Repository::MODE;
		}

		if (is_null($file)) {
			$file = Repository::FILE;
		}

		$this->assertFalse($repo->offsetExists($path));

		try {
			$repo->offsetSet($path, null);
		} catch (InvalidArgumentException $exception) {
			$this->assertSame(
				'Expected value to be a PSR-compliant server request',
				$exception->getMessage()
			);
		}

		$this->assertNotNull($exception);
		$exception = null;

		$previous = $root->getPermissions();
		$root->chmod(0);

		try {
			$repo->offsetSet($path, $request);
		} catch (RuntimeException $exception) {
			$this->assertSame(
				"Cannot create directory '{$trimmed}'",
				$exception->getMessage()
			);
		}

		$this->assertNotNull($exception);
		$exception = null;

		$root->chmod($previous);

		$repo->offsetSet($path, $request);

		$this->assertTrue($repo->offsetExists($path));

		$this->assertSame($mode + 0x4000, fileperms($trimmed));
		$this->assertSame($content, file_get_contents($in));

		($repo->offsetGet($path))($builder, $context);
		$this->assertInvoked($invoked);

		$repo->offsetUnset($path);
		$this->assertFalse($repo->offsetExists($path));

		$basename = basename(__FILE__);
		$destination = "{$base}/{$basename}";

		$extended = $this->withUploads(
			$request,
			$destination,
			$file,
			UPLOAD_ERR_EXTENSION
		);

		try {
			$repo->offsetSet($path, $extended);
		} catch (UnexpectedValueException $exception) {

			$this->assertSame(
				'An unrecoverable error is associated with the uploaded file',
				$exception->getMessage()
			);

			$this->assertSame(UPLOAD_ERR_EXTENSION, $exception->getCode());
		}

		$this->assertNotNull($exception);
		$exception = null;

		$nofile = $this->withUploads(
			$request,
			$destination,
			$file ?? Repository::FILE,
			UPLOAD_ERR_NO_FILE
		);

		$repo->offsetSet($path, $nofile);

		$this->assertTrue($repo->offsetExists($path));

		$this->assertSame($content, file_get_contents($in));

		($repo->offsetGet($path))($builder, $context);
		$this->assertInvoked($invoked);

		$repo->offsetUnset($path);
		$this->assertFalse($repo->offsetExists($path));

		$upload = $this->withUploads($request, $destination, $file);

		$repo->offsetSet($path, $upload);

		$this->assertTrue($repo->offsetExists($path));

		$content = file_get_contents(__FILE__);
		$this->assertNotFalse($content);
		$this->assertSame($content, file_get_contents($in));

		($repo->offsetGet($path))($builder, $context);
		$this->assertInvoked($invoked);

		$repo->offsetUnset($path);
		$this->assertFalse($repo->offsetExists($path));
	}

	private function assertDirectory(
		string $root,
		string $extension = null
	): void {
		$directory = vfsStream::setup($root);
		$prefix = "{$directory->url()}/";
		$repository = new Repository($this, $prefix, $extension);
		$actual = $repository->directory();
		DirectoryHelper::assert($this, $actual, '/', $prefix, $extension);
	}

	private function assertInvoked(bool &$invoked)
	{
		$this->assertTrue($invoked);
		$invoked = false;
	}

	private function withUploads(
		ServerRequest $request,
		string $destination,
		string $key,
		int $error = UPLOAD_ERR_OK
	): ServerRequest {


		$this->assertTrue(copy(__FILE__, $destination));
		$this->assertTrue(file_exists($destination));

		$upload = new UploadedFile($destination, filesize($destination), $error);
		return $request->withUploadedFiles([$key => $upload]);
	}
}
