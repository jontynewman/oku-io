<?php

namespace JontyNewman\Oku\IO;

use JontyNewman\Oku\ContextInterface;
use JontyNewman\Oku\Directory;
use JontyNewman\Oku\File;
use JontyNewman\Oku\ResponseBuilderInterface;
use RuntimeException;
use ShrooPHP\PSR\Requests\ServerRequestAdapter\Converters\ShiftConverter;
use Throwable;

/**
 * A cache of outputs from set inputs.
 */
class Cache
{
	/**
	 * The callback being used to generate output.
	 *
	 * @var callable
	 */
	private $callback;

	/**
	 * The directory being used to cache output.
	 *
	 * @var \JontyNewman\Oku\Directory
	 */
	private $directory;

	/**
	 * Constructs a cache of outputs from set inputs.
	 *
	 * The callback will be passed the path of the input file as the first
	 * argument and the expected path of the output file as the second.
	 *
	 * @param callable $callback The callback to use in order to generate
	 * output.
	 * @param string $prefix The prefix to prepend to cached output files.
	 * @param string $extension The extension to append to cached output files
	 * (if any).
	 */
	public function __construct(
		callable $callback,
		string $prefix,
		string $extension = null
	) {

		$this->callback = $callback;
		$this->directory = new Directory($prefix, $extension);
	}

	/**
	 * Gets the directory associated with the cache.
	 *
	 * @return \JontyNewman\Oku\Directory The directory associated with the
	 * cache.
	 */
	public function directory(): Directory
	{
		return $this->directory;
	}

	/**
	 * Caches the output of the given path using the given input file.
	 *
	 * This is unless the modification time of the output is greater than that
	 * of the input file, or if caching is being forced using the associated
	 * parameter.
	 *
	 * @param string $path The path to cache.
	 * @param string $in The input file to use in order to generate the output.
	 * @param bool $force Whether or not modification times should be ignored.
	 * @return string The path of the output file.
	 */
	public function cache(string $path, string $in, bool $force = false): string
	{
		$out = $this->directory()->offsetGet($path);

		if ($force || !file_exists($out) || $this->expired($in, $out)) {
			($this->callback)($in, $out);
		}

		return $out;
	}

	/**
	 * Builds a response using the given response builder.
	 *
	 * @param string $path The path of the request.
	 * @param string $in The path of the input file.
	 * @param \JontyNewman\Oku\ResponseBuilderInterface $builder The builder to
	 * use in order to build the response.
	 * @param \JontyNewman\Oku\ContextInterface $context The current context.
	 * @throws \RuntimeException The response could not be built.
	 */
	public function __invoke(
		string $path,
		string $in,
		ResponseBuilderInterface $builder,
		ContextInterface $context
	): void {

		$headers = $context->request()->getHeaders();
		$converted = (new ShiftConverter())->convert($headers);
		$out = $this->cache($path, $in);

		if (!File::build($builder, $out, $converted)) {
			throw new RuntimeException("Unable to build response from '{$out}'");
		}
	}

	/**
	 * Determines whether or not the given input file has been modified since
	 * the given output file was modified.
	 *
	 * @param string $in The path of the input file to evaluate.
	 * @param string $out The path of the output file to evaluate.
	 * @return bool Whether or not the input file has been modified since the
	 * output file was modified.
	 */
	private function expired(string $in, string $out): bool
	{
		return $this->mtime($in) > $this->mtime($out);
	}

	/**
	 * Converts the given file path to a modification time.
	 *
	 * @param string $path The file path to convert.
	 * @return int The converted file path.
	 * @throws \RuntimeException The modification time could not be determined.
	 */
	private function mtime(string $path): int
	{
		$previous = null;
		$mtime = false;

		try {
			$mtime = filemtime($path);
		} catch (Throwable $previous) {
			// Do nothing (maintain the modification time as FALSE).
		}

		if (false === $mtime) {
			throw new RuntimeException(
				"Cannot determine modification time for '{$path}'",
				0,
				$previous
			);
		}

		return $mtime;
	}
}
