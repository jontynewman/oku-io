<?php

namespace JontyNewman\Oku\IO;

use ArrayAccess;
use InvalidArgumentException;
use JontyNewman\Oku\ContextInterface;
use JontyNewman\Oku\Directory;
use JontyNewman\Oku\ResponseBuilderInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UploadedFileInterface;
use RuntimeException;
use UnexpectedValueException;

/**
 * A repository of input files.
 */
class Repository implements ArrayAccess
{
	/**
	 * The default mode of created directories.
	 */
	const MODE = 0770;

	/**
	 * The default key of uploaded files.
	 */
	const FILE = 'io-file';

	/**
	 * The default key of direct input.
	 */
	const TEXT = 'io-text';

	/**
	 * The callback being used to build responses.
	 *
	 * @var callable
	 */
	private $callback;

	/**
	 * The directory being used to persist input files.
	 *
	 * @var \JontyNewman\Oku\Directory
	 */
	private $directory;

	/**
	 * The key currently associated with uploads.
	 *
	 * @var string
	 */
	private $file;

	/**
	 * The key currently associated with direct input.
	 *
	 * @var string
	 */
	private $text;

	/**
	 * Constructs a repository of input files that are processed in order to
	 * build associated responses.
	 *
	 * The callback will be passed the request path as the first argument, the
	 * path of the input file as the second argument, the response builder as
	 * the third argument, and the current context as the fourth.
	 *
	 * @param callable $callback The callback to use in order to build
	 * responses.
	 * @param string $prefix The prefix to prepend to persisted input files.
	 * @param string $extension The extension to append to persisted input
	 * files.
	 * @param int $mode The mode to associate with created directories.
	 * @param string $file The key to associate with uploads.
	 * @param string $text The key to associate with direct input.
	 */
	public function __construct(
		callable $callback,
		string $prefix,
		string $extension = null,
		int $mode = null,
		string $file = null,
		string $text = null
	) {

		$this->callback = $callback;
		$this->directory = new Directory($prefix, $extension);
		$this->mode = $mode ?? self::MODE;
		$this->file = $file ?? self::FILE;
		$this->text = $text ?? self::TEXT;
	}

	/**
	 * Gets the directory associated with the repository.
	 *
	 * @return \JontyNewman\Oku\Directory The directory associated with the
	 * repository.
	 */
	public function directory(): Directory
	{
		return $this->directory;
	}

	public function offsetExists($offset): bool
	{
		return $this->directory()->offsetExists($offset);
	}

	public function offsetGet($offset)
	{
		return function (
			ResponseBuilderInterface $builder,
			ContextInterface $context
		) use ($offset): void {

			$path = $this->directory()->offsetGet($offset);

			($this->callback)((string) $offset, $path, $builder, $context);
		};
	}

	public function offsetSet($offset, $value): void
	{
		$path = $this->directory()->offsetGet($offset);
		$parent = dirname($path);

		if (!($value instanceof ServerRequestInterface)) {
			throw new InvalidArgumentException('Expected value to be a PSR-compliant server request');
		}

		$upload = $value->getUploadedFiles()[$this->file] ?? null;

		if (!file_exists($parent)) {

			if (!mkdir($parent, $this->mode, true)) {
				throw new RuntimeException("Cannot create directory '{$parent}'");
			}
		}

		if (is_null($upload) || !$this->move($upload, $path)) {
			$this->directory()->offsetSet(
				$offset,
				(string) $value->getParsedBody()[$this->text] ?? ''
			);
		}
	}

	public function offsetUnset($offset): void
	{
		$this->directory()->offsetUnset($offset);
	}

	/**
	 * Moves the given uploaded file the to the given path.
	 *
	 * @param UploadedFileInterface $upload The uploaded file to move.
	 * @param string $path The destination of the uploaded file.
	 * @return bool Whether or not the file was moved.
	 * @throws \UnexpectedValueException The error associated with the upload
	 * could not be resolved.
	 */
	private function move(UploadedFileInterface $upload, string $path): bool
	{
		$moved = false;
		$error = $upload->getError();

		switch ($error) {
			case UPLOAD_ERR_OK:
				$upload->moveTo($path);
				$moved = true;
				break;
			case UPLOAD_ERR_NO_FILE:
				// Do nothing.
				break;
			default:
				throw new UnexpectedValueException(
					'An unrecoverable error is associated with the uploaded file',
					$error
				);
		}

		return $moved;
	}
}
