# JontyNewman\Oku\IO

Functionality for generating output from set input per path.

For example, a given path could take set reStructuredText as input and produce
cached HTML as output.

## Installation

```
composer require 'jontynewman/oku-io ^1.0'
```

## Example Usage

The following example assumes that
[`rst2html5`](http://docutils.sourceforge.net/docs/user/tools.html#rst2html5-py)
is available, and that the following packages are installed.

- [`jontynewman/html-filter`](https://gitlab.com/jontynewman/html-filter)
- [`jontynewman/oku-io`](https://gitlab.com/jontynewman/oku-io)
- [`jontynewman/oku-proc`](https://gitlab.com/jontynewman/oku-proc)

The editor will allow users to either input or upload reStructuredText in order
to create static web pages.

```php
<?php

use GuzzleHttp\Psr7\Response;
use JontyNewman\HtmlFilter;
use JontyNewman\Oku\ContextInterface;
use JontyNewman\Oku\IO\Cache;
use JontyNewman\Oku\IO\Repository;
use JontyNewman\Oku\Process;
use JontyNewman\Oku\RequestHandler;
use JontyNewman\Oku\ResponseBuilderInterface;

require 'vendor/autoload.php';

$dir = '/path/to/io/directory';

// Convert reStructuredText input files to HTML output files.
$process = new Process('rst2html5');

// Cache HTML output in the specified directory.
$cache = new Cache($process, "{$dir}/html/", 'html');

// Persist reStructuredText input in the specified directory (for future edits).
$repository = new Repository($cache, "{$dir}/rst/", 'rst');

// Allow users to edit and upload reStructuredText.
$editor = function (
	ResponseBuilderInterface $builder,
	ContextInterface $context
) use ($repository): void {

	$path = $context->request()->getUri()->getPath();
	$in = $repository->directory()->offsetGet($path);
	$render = function () use ($in) {

		if (file_exists($in)) {
			HtmlFilter::passthru($in, 'rb');
		}
	};

	$builder->content(function () use ($render, $context) {
		require 'editor.php';
	});
};

// Use a simple 404 page as the default response.
$default = new Response(404, ['Content-Type' => 'text/plain'], 'Not Found');

// Set up the request handler.
$handler = new RequestHandler($repository, $default, $editor);

// Run the application.
$handler->run();

```

An example template of the editor (i.e. `editor.php`) follows.

```php
<?php

use JontyNewman\Oku\IO\Repository;
use JontyNewman\Oku\Helpers\Html;

/* @var $render callable */
/* @var $context \JontyNewman\Oku\ContextInterface */

?>
<!DOCTYPE html>
<html>
  <head>
    <title>JontyNewman\Oku\IO</title>
  </head>
  <body>
    <form action="" method="post" enctype="multipart/form-data">
      <p>
        <label>
          File
          <input type="file" name="<?= Html::escape(Repository::FILE); ?>">
        </label>
      </p>
      <p>
        <label>
          Text
          <textarea name="<?= Html::escape(Repository::TEXT); ?>"><?php ($render)(); ?></textarea>
        </label>
      </p>
      <p>
        <?= $context->token(); ?>
        <?= $context->put(); ?>
        <input type="submit" value="Save">
      </p>
    </form>
    <form action="" method="post">
      <p>
        <?= $context->token(); ?>
        <?= $context->delete(); ?>
        <input type="submit" value="Delete">
      </p>
    </form>
    <?= $context->inset(); ?>
  </body>
</html>

```
