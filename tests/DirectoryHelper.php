<?php

namespace JontyNewman\Oku\IO\Tests;

use PHPUnit\Framework\TestCase;
use JontyNewman\Oku\Directory;

class DirectoryHelper
{
	public static function assert(
		TestCase $test,
		Directory $actual,
		string $offset,
		string $prefix,
		string $extension = null
	): void {

		$contents = __METHOD__;
		$expected = new Directory($prefix, $extension);

		self::unset($test, $expected, $actual, $offset);

		$actual->offsetSet($offset, $contents);

		$after = $expected->offsetExists($offset);

		$test->assertTrue($after);
		$test->assertSame($after, $actual->offsetExists($offset));

		$path = $expected->offsetGet($offset);

		$test->assertSame($path, $actual->offsetGet($offset));

		$test->assertSame($contents, file_get_contents($path));

		self::unset($test, $expected, $actual, $offset);
	}

	private static function unset(
		TestCase $test,
		Directory $expected,
		Directory $actual,
		string $offset
	): void {

		$actual->offsetUnset($offset);

		$exists = $actual->offsetExists($offset);

		$test->assertFalse($exists);
		$test->assertSame($expected->offsetExists($offset), $exists);
	}
}
